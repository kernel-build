platform=imx7
dir_pattern=plusoptix
arch=arm
dtbs=(
	imx7d-sx-pl-emar.dtb
	imx7d-sx-pl-test.dtb
	imx7d-sx-imx7-pl.dtb
)
image=zImage
loadaddr=0x80008000
# For as long as we're stuck with the BSP
extra_kcflags[4.9.87]="-Wno-error"
