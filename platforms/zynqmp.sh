platform=zynqmp
dir_pattern=xilinx
arch=arm64
dtbs=(xilinx/zynqmp-zcu106-revA.dtb)
image=FIT
loadaddr=0x00080000
kcflags="$kcflags -Wno-error=packed-not-aligned -Wno-error=sizeof-pointer-memaccess -Wno-error=stringop-truncation -Wno-error=stringop-overflow"
target_dir=$HOME/src/netboot/$platform
