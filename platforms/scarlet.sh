platform=scarlet
arch=arm64
cmdline="console=ttyS2,115200n8 earlyprintk=ttyS2,115200n8 console=tty1 init=/sbin/init root=PARTUUID=%U/PARTNROFF=1 rootwait rw noinitrd ignore_loglevel"
cmdline="console=ttyS2,115200n8 earlyprintk=ttyS2,115200n8 console=tty1 ip=dhcp root=/dev/nfs rootwait rw noinitrd ignore_loglevel"
dtbs=(rockchip/rk3399-gru-scarlet-inx.dtb)
image=CrOS
loadaddr=0

flash_parts_num=12
declare -A flash_parts
flash_parts[kernel]="2;KERN-A;RAW"
flash_parts[modules]="3;ROOT-A;/lib/modules"
