platform=gen3
dir_pattern=renesas
arch=arm64
dtbs=(
	renesas/r8a7795-es1-h3ulcb.dtb
	renesas/r8a7795-es1-salvator-x.dtb
	renesas/r8a7795-h3ulcb.dtb
	renesas/r8a7795-h3ulcb-kf.dtb
	renesas/r8a7795-salvator-x.dtb
	renesas/r8a7795-salvator-xs.dtb
	renesas/r8a7796-m3ulcb.dtb
	renesas/r8a7796-salvator-x.dtb
	renesas/r8a77995-draak.dtb
)
dtbs=($(cd arch/arm64/boot/dts ; ls renesas/*.dts | sed 's/\.dts/.dtb/'))
#dtbs=(renesas/r8a77965-salvator-xs.dtb)
image=FIT
loadaddr=0x48080000
