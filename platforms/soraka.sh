platform=soraka
dir_pattern=libcamera
arch=x86
cmdline="console=ttyS0,115200n8 earlyprintk=ttyS0,115200n8 console=tty1 ip=dhcp root=/dev/nfs rootwait rw noinitrd ignore_loglevel"
image=CrOS
kcflags="$kcflags -Wno-error=frame-larger-than="

flash_parts_num=12
declare -A flash_parts
flash_parts[kernel]="2;KERN-A;RAW"
flash_parts[modules]="3;ROOT-A;/lib/modules"
