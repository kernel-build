#
# Cross-compilers
#
# Variable name: cross_compile[]
#
cross_compile[arm]=arm-buildroot-linux-uclibcgnueabihf-
cross_compile[arm64]=aarch64-buildroot-linux-gnu-

#
# Directories
#
# Variable name: flash_mount_point
#
# Path to a directory where the target disk can be mounted.
#
flash_mount_point=/mnt/disk
#
# Variable name: target_root[]
#
# Path to the root directory of the target file system for each architecture.
# If set, the kernel image and DTBs are installed to $target_root[$arch]/boot,
# and the kernel modules to $target_root[$arch]/lib/modules. Platform files can
# override the NFS root by setting $target_dir.
#
target_root[arm]=$HOME/src/netboot/arm32
target_root[arm64]=$HOME/src/netboot/arm64
target_root[x86]=$HOME/src/netboot/x86
#
# Variable name: tftp_root
#
# Path to the root directory of the TFTP server. If set, the kernel image and
# DTBs are installed to $tftp_root/$platform if the directory exists.
#
tftp_root=$HOME/tftpboot
