# SPDX-License-Identifier: GPL-2.0+

make_srec_image() {
	local image=$output_dir/arch/$arch/boot/$image_kernel

	cat $image | lzma -z -e > $image.xz
	objcopy -I binary -O srec --srec-forceS3 --srec-len 516 \
		$image.xz $image.xz.srec

	for dtb in ${dtbs[0]} ; do
		local src=${dtb/:*/}

		objcopy -I binary -O srec --srec-forceS3 --srec-len 516 \
			$src $src.srec
	done
}

_image_file=$image_kernel.xz.srec
