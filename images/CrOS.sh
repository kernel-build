# SPDX-License-Identifier: GPL-2.0+

if [[ $arch != arm64 && $arch != x86 ]] ; then
	echo "CrOS image unsupported on architecture $arch"
	exit 1
fi

source "${kbuild_root}/images/FIT.sh" || exit 1

make_CrOS_image() {
	local boot_dir=$output_dir/arch/$arch/boot
	local bootloader
	local config
	local vmlinuz

	if [[ $arch == arm64 ]] ; then
		make_FIT_image
		vmlinuz=$boot_dir/kernel_fdt.itb
	elif [[ $arch == x86 ]] ; then
		vmlinuz=$boot_dir/bzImage
	fi

	if [[ -f $opt_bootloader ]] ; then
		bootloader=$opt_bootloader
	else
		dd if=/dev/zero of=$boot_dir/bootloader bs=512 count=1
		bootloader=$boot_dir/bootloader
	fi

	if [[ -f $opt_cmdline_file ]] ; then
		config=$opt_cmdline_file
	else
		echo "$cmdline" > $boot_dir/cmdline
		config=$boot_dir/cmdline
	fi

	vbutil_kernel \
		--pack $boot_dir/vmlinuz.image \
		--version 1 \
		--vmlinuz $vmlinuz \
		--arch $arch \
		--keyblock /usr/share/vboot/devkeys/kernel.keyblock \
		--signprivate /usr/share/vboot/devkeys/kernel_data_key.vbprivk \
		--config $config \
		--bootloader $bootloader
}

_image_file=vmlinuz.image
