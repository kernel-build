# SPDX-License-Identifier: GPL-2.0+

make_uImage_image() {
	mkimage -A $arch -O linux -T kernel -C none -a $loadaddr -e $loadaddr \
		-n "Linux-${version}" -d $output_dir/arch/$arch/boot/$image_kernel \
		$output_dir/arch/$arch/boot/uImage
}

_image_file=uImage
